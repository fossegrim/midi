module gitlab.com/gomidi/midi/player/example/smfplayer

go 1.12

require (
	gitlab.com/gomidi/rtmididrv v0.10.1
	gitlab.com/metakeule/config v1.13.0
	gitlab.com/gomidi/midi v1.17.0
)

replace gitlab.com/gomidi/midi => ../../..